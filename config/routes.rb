Rails.application.routes.draw do
  root "homes#index"

  devise_for :users
  resources :users
  resources :blogs
  resources :comments

  resources :blogs do
    resources :likes
  end

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
