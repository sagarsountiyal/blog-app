class CommentsController < ApplicationController
 
  def new
    @comment = Comment.new
  end
 
  def create
    @comment = current_user.comments.new(comment_params)
    if @comment.save
       	flash[:success] = "Commented successfully!"
    else
        flash[:error] = @comment.errors.full_messages.to_sentence
    end
    redirect_to root_path
  end
 
  private
 
  def comment_params
    params.require(:comment).permit(:text, :blog_id, :text, :comment_id)
  end
 
end