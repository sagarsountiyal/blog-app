class BlogsController < ApplicationController
 
  def new
    @blog = Blog.new
  end
 
  def create
    @blog = current_user.blogs.new(blog_params)
    if @blog.save
       	flash[:success] = "Blog Created Successfully!"
    else
        flash[:error] = @blog.errors.full_messages.to_sentence
    end
    redirect_to root_path
  end
 
  private
 
  def blog_params
    params.require(:blog).permit(:text, :image)
  end
 
end