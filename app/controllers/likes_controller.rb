class LikesController < ApplicationController
 	
	before_action :find_blog

	def index
		@likes = Like.joins(:user, :blog).select("users.email as email").where("blogs.id = ?", @blog.id)
		render json: {status: true, liked_by: @likes.map(&:email)}, status: 200
	end

  	def create
  		if already_liked?
  			flash[:alert] = "Post Already Liked"
  		else
  			@blog.likes.create(:user_id => current_user.id)
  		end
  		redirect_to root_path
  	end
 
  	private
 
  	def find_blog
    	@blog = Blog.find(params[:blog_id])
  	end
 	
 	def already_liked?
 		Like.where(:user_id => current_user.id, :blog_id => @blog.id).exists?
 	end
end