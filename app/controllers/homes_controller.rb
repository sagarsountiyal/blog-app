class HomesController < ApplicationController
  def index
    @blogs = Blog.joins(:user).left_joins(:likes).select("blogs.*, users.email as email, COUNT(likes.id) AS likes_count").order("created_at DESC").group("blogs.id")
    @all_comments = Comment.all_comments
  end
end
