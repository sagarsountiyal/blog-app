class Blog < ApplicationRecord

	belongs_to :user

	has_many :comments, dependent: :destroy
	has_many :likes, dependent: :destroy
	
	validates_presence_of   :text, :image

	# validates             :image, :file_size => { less_than_or_equal_to: 10.megabyte }
  	mount_uploader :image, BlogUploader

end
