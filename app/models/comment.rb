class Comment < ApplicationRecord
	belongs_to :blog
	belongs_to :user

	validates_presence_of   :text

	# Return all comments row
	scope :all_comments, -> {
		joins(:user).select('comments.*, users.name as sender_name, users.email as sender_email')
	}

	#Return all comments of the blog
	scope :get_blog_comments, ->(blog_id, all_comments) {
		all_comments.select{ |a| a.comment_id.nil? && a.blog_id == blog_id }
	}

	# Return all replies of the particular Comment
	def get_replies(all_comments)
		all_comments.select{ |a| a.comment_id == self.id }
	end

end
