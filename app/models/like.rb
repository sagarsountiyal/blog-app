class Like < ApplicationRecord
	validates_presence_of   :user_id, :blog_id
	belongs_to :user
	belongs_to :blog
end
