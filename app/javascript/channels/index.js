// Load all the channels within this directory and all subdirectories.
// Channel files must be named *_channel.js.

const channels = require.context('.', true, /_channel\.js$/)
channels.keys().forEach(channels)


$(document).ready(function() {
    $("#myBtn").click(function(e) {
        var blog_id = $(this).data('id');
        $.ajax({
            type: "GET",
            beforeSend: setCsrfToken, // implemented in application.html.erb
            url: "/blogs/" + blog_id + "/likes",
            success: function(data) {
                var users = data['liked_by']
                jQuery.each(users, function(i, val) {
                    $('#liked_user_list').append(val + "<br><br>")
                });

            },
            error: function(exception) {
                notification("Somethin went wrong", "alert");
            }
        });
    });
});